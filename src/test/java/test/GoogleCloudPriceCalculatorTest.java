package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import pages.GoogleCloudHomePage;
import pages.GoogleCloudPricingCalculatorPage;



public class GoogleCloudPriceCalculatorTest {
    private WebDriver driver;
    private WebDriver yopmailDriver;

    @Before
    public void browserSetup() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testCloudPricingCalculate() throws InterruptedException {
        //open google cloud homepage and search calculator, open link and fill the fields
        GoogleCloudHomePage homePage = new GoogleCloudHomePage(driver);
        GoogleCloudPricingCalculatorPage calculatorPage = homePage.openPage()
                .searchTerm("Google Cloud Platform Pricing Calculator")
                .openLinkWithMatch()
                .setFields();
        //open yopmail
        /*yopmailDriver = new ChromeDriver();
        yopmailDriver.manage().window().maximize();
        YopmailHomePage yopmailHomePage = new YopmailHomePage(yopmailDriver);
        //generating random email
        String email = yopmailHomePage.generateRandomEmail();
        //send estimate sum to generated email
        calculatorPage.sendEstimateEmail(email);
        //verify email
        String title = yopmailHomePage.checkInbox().getFirstEmailTitle();
        System.out.println("title:"+title);
        assertEquals(title, "Google Cloud Price Estimate");*/
    }

    @After
    public void cleanUp() {
        driver.quit();
    }
}
