package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class YopmailHomePage {
    private WebDriver driver;
    private Wait<WebDriver> wait;
    private static final String HOMEPAGE_URL = "https://yopmail.com/";

    //checkInbox
    private By checkInboxButton = By.xpath("//div[@class='nw']/button[2]");
    //generateEmail
    private By generateEmailLink = By.xpath("//div[@id='listeliens']/a[1]");
    //emailAddress
    private By emailAddressText = By.xpath("//*[@id='geny']/span[1]");


    public YopmailHomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(HOMEPAGE_URL);
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);
    }

    public String generateRandomEmail(){
        wait.until(ExpectedConditions.elementToBeClickable(generateEmailLink)).click();

        // trying to close the add
        try {
            driver.switchTo().defaultContent();
            //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//iframe[@id='ad_iframe']")));
            //driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='ad_iframe']")));
           // wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='dismiss-button']"))).click();
        }catch (TimeoutException exception){
            System.out.println(exception.getMessage());
        }

        WebElement emailAddress = wait.until(
                ExpectedConditions.presenceOfElementLocated(emailAddressText));
        System.out.println("email:" + emailAddress.getText());
        return emailAddress.getText()+"@yopmail.com";
    }

    public YopmailInboxPage checkInbox(){
        driver.findElement(checkInboxButton).click();
        return new YopmailInboxPage(driver);
    }
}
