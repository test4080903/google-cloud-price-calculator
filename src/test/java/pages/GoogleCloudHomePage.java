package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudHomePage {
    private static final String HOMEPAGE_URL = "https://cloud.google.com/";
    private WebDriver driver;

    @FindBy(xpath = "//div[@class = 'YSM5S']")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@class='qdOxv-fmcmS-wGMbrd']")
    private WebElement searchInput;

    @FindBy(xpath = "//label[@jsname = 'vhZMvf']/i[@jsname='Z5wyCf']")
    private WebElement findButton;

    public GoogleCloudHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GoogleCloudHomePage openPage(){
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver, Duration.ofSeconds(4))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class = 'YSM5S']")));
        return this;
    }
    public GoogleCloudSearchResultsPage searchTerm(String term){
        searchIcon.click();
        searchInput.sendKeys(new String[]{term});
        findButton.click();
        return new GoogleCloudSearchResultsPage(driver, term);
    }
}
