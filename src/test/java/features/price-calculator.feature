Feature: Google Cloud Price Calculator automation
  Scenario: Confirm estimate calculated and email has sent
    Given Google Cloud Price Calculator is opened through Google Cloud Home Page
    When All necessary fields are set
    And Estimate is calculated
    And Random Email is generated
    And Estimate is sent to that email
    Then Email with estimated price should be received