package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudSearchResultsPage {
    private WebDriver driver;
    private String term;

    public GoogleCloudSearchResultsPage(WebDriver driver, String term) {
        this.driver = driver;
        this.term = term;
        new WebDriverWait(driver, Duration.ofSeconds(5))
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//div[@jsname='J6ONMd']")));
    }

    public GoogleCloudPricingCalculatorPage openLinkWithMatch(){
        String searchXPath = "//div[@jsname='J6ONMd']//a[contains(text(),'" + term + "')]";
        //WebElement firstMatch = driver.findElement(By.xpath(searchXPath));
        driver.get("https://cloudpricingcalculator.appspot.com/");
        return new GoogleCloudPricingCalculatorPage(driver);
    }
}
