package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.GoogleCloudHomePage;
import pages.GoogleCloudPricingCalculatorPage;
import pages.YopmailHomePage;

import java.util.Set;

import static org.testng.Assert.assertEquals;

public class PriceCalculatorTest {

    private WebDriver driver;
    private GoogleCloudPricingCalculatorPage pricingCalculatorPage;
    private YopmailHomePage yopmailHomePage;
    private String email;
    Set<String> windowHandles;


    @Given("Google Cloud Price Calculator is opened through Google Cloud Home Page")
    public void google_cloud_price_calculator_is_opened_through_google_cloud_home_page() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pricingCalculatorPage = new GoogleCloudHomePage(driver)
                .openPage()
                .searchTerm("Google Cloud Platform Pricing Calculator")
                .openLinkWithMatch();
    }
    @When("All necessary fields are set")
    public void all_necessary_fields_are_set() {
        pricingCalculatorPage.setFields();
    }
    @When("Estimate is calculated")
    public void estimate_is_calculated() {
        pricingCalculatorPage.clickAddToEstimate();
    }
    @When("Random Email is generated")
    public void random_email_is_generated() {
        driver.switchTo().newWindow(WindowType.TAB);
        yopmailHomePage = new YopmailHomePage(driver);
        email = yopmailHomePage.generateRandomEmail();
    }
    @When("Estimate is sent to that email")
    public void estimate_is_sent_to_that_email() {
        windowHandles = driver.getWindowHandles();
        String firstTabHandle = windowHandles.toArray()[0].toString();
        driver.switchTo().window(firstTabHandle);
        pricingCalculatorPage.sendEstimateEmail(email);
    }
    @Then("Email with estimated price should be received")
    public void email_with_estimated_price_should_be_received() {
        String secondTabHandle = windowHandles.toArray()[1].toString();
        driver.switchTo().window(secondTabHandle);
        String title = yopmailHomePage.checkInbox().getFirstEmailTitle();
        System.out.println("title:"+title);
        assertEquals(title, "Google Cloud Price Estimate");
        driver.quit();
    }

}
